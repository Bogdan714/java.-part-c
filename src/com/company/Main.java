package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("\nTask 1");
        System.out.println("Ввести n строк с консоли, найти самую короткую строку. Вывести эту строку и ее длину.\n");

        System.out.println("Сколько строк?");
        int quantityOfRows = Integer.valueOf(in.nextLine());
        String[] rows = new String[quantityOfRows];
        int shortestId = 0;
        for (int i = 0; i < rows.length; i++) {
            System.out.print("Строка " + (i + 1) + ": ");
            rows[i] = in.nextLine();
            if(rows[shortestId].length() > rows[i].length()){
                shortestId = i;
            }
        }

        System.out.println("Самая короткая строка: \n"
                + rows[shortestId] + "\n"
                + "Длина строки: " + rows[shortestId].length());

        System.out.println("\nTask 2");
        System.out.println("Ввести n строк с консоли. Вывести на консоль те строки, длина которых меньше средней.\n");

        System.out.println("Сколько строк?");
        int quantityOfRows2 = Integer.valueOf(in.nextLine());
        String[] rows2 = new String[quantityOfRows2];
        int lengthTotal = 0;
        for (int i = 0; i < rows2.length; i++) {
            System.out.print("Строка " + (i + 1) + ": ");
            rows2[i] = in.nextLine();
            lengthTotal += rows2[i].length();
        }

        float average = (float)lengthTotal/quantityOfRows2;
        //System.out.println("Средняя длина строки: " + average);
        System.out.println("Строки с длиной меньше средней:");
        for (String item : rows2) {
            if(item.length() < average){
                System.out.println(item);
            }
        }

        System.out.println("\nTask 3");
        System.out.println("Ввести n строк с консоли.\n" +
                "Вывести на консоль те строки, которые содержат одно из\n" +
                "заданных ключевых слов (ключевые слова заданы изначально,\n" +
                "в массиве например “tree”, “meat”).\n");

        String[] keywords = {"xxx", "free", "hot"};
        System.out.println("Ключевые слова: ");
        for (String keyword : keywords) {
            System.out.print(keyword + " ");
        }
        System.out.println("\nСколько строк?");
        int quantityOfRows3 = Integer.valueOf(in.nextLine());
        String[] rows3 = new String[quantityOfRows3];
        for (int i = 0; i < rows3.length; i++) {
            System.out.print("Строка " + (i + 1) + ": ");
            rows3[i] = in.nextLine().toLowerCase();
        }

        System.out.println("Строки с ключевыми словами:");
        for (String item : rows3) {
            for (String keyword : keywords) {
                if(item.contains(keyword)){
                    System.out.println(item);
                    break;
                }
            }
        }

        System.out.println("\nTask 4");
        System.out.println("Ввести с консоли несколько предложений. Удалить в них\nвсе слова заданной длины, начинающиеся на согласную букву");
        String[] consonants = {"б", "в", "г", "д", "ж", "з", "й", "к", "л", "м", "н", "п", "р", "с", "т", "ф", "х", "ц", "ч", "ш", "щ"};
        System.out.println("Введите несколько предложений: ");
        String string = in.nextLine();
        System.out.print("Введите длину удаляемого слова: ");
        int wordLen = Integer.valueOf(in.nextLine());
        String[] words = string.split(" ");
        for (int i = 0; i < words.length; i++) {
            if(words[i].length() == wordLen) {
                for (String consonant : consonants) {
                    if (words[i].toLowerCase().startsWith(consonant)) {
                        words[i] = "";
                        break;
                    }
                }
            }
        }

        System.out.println("Форматированное предложение:");
        for (String word : words) {
            System.out.print(word + (word.equals("") ? "" : " "));
        }


    }
}
